FROM tobi312/rpi-nginx:latest

RUN rm -rf /var/www/html

# copy artifact build from the 'build environment'
COPY /dist/newSnack /var/www/html

# expose port 80
EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]